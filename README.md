# IMPORTANT NOTICE

* R package `lidaRtRee` has moved to [https://forgemia.inra.fr/lidar/lidaRtRee](https://forgemia.inra.fr/lidar/lidaRtRee)
* Documentation and tutorials are on [https://lidar.pages.mia.inra.fr/lidaRtRee/](https://lidar.pages.mia.inra.fr/lidaRtRee/)